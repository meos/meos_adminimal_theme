# Git Flow Hooks

## After cloning this repository run:

```
bin/gitflowhooksinit
```

to setup the Git Flow hooks and configuration to the repository settings.

## Git Flow Hooks Configuration
* The Git and Git Flow hooks are in the `.githooks` folder.
* Git Flow config is in the `.gitflowconfig` in the root of the repository, but needs to be included in the local git config after cloning.
* Configuration for the Git Flow hooks is in the `git-flow-hooks-config.sh`, but needs to be copied into the `.git/` directory after cloning the repository.
* In the `bin` directory is a script that will take care of including the Git Flow config and copying the configuration for the Git Flow hooks to the `.git` directory.

Git flow hooks can be saved in .githooks in the root of the repository. It include these you can include the following config in the .gitflowconfig from above.


## .gitflowconfig

The following configuration will be included from `.gitflowconfig`

This adds the Git Hooks.

```
[gitflow "path"]
	hooks = .githooks/
```

The following configuration is for the automatic messages for the tag generation using the version number.

```
[gitflow "hotfix.finish"]
	message = %tag%
[gitflow "release.finish"]
	message = %tag%
```

Sample `.git/gitflowhooks-config.sh`


```
VERSION_FILE="version.md"
VERSION_SORT="/usr/bin/sort -V"
VERSION_BUMPLEVEL_HOTFIX="PATCH"
VERSION_BUMPLEVEL_RELEASE="PATCH"
VERSION_BUMP_MESSAGE="Bumping version to %version%"
VERSION_TAG_PLACEHOLDER="%tag%"
```

Sample `.gitflowconfig` file.


```
[gitflow "branch"]
	master = live
	develop = dev
[gitflow "prefix"]
	feature = 
	bugfix = 
	release = 
	hotfix = 
	support = 
	versiontag = 
[gitflow "path"]
	hooks = .githooks/
[gitflow "hotfix.finish"]
	message = %tag%
[gitflow "release.finish"]
	message = %tag%
```

## Additional  information
Git configuration is stored in system (computer), global (user), and local (repository) files.

In the global, which can be accessed with the --global option of the git config command, we store our personal setting, username, tool settings, git settings that we prefer for our workflow. These settings can be overridden or added to (in the case of settings that take multiple options) by the settings in the local (repository). 

However it seems (I cannot find any documentation to confirm this.) that some settings that are saved in the local configuration are only locally saved. It may be that the this the config file  is generated.
