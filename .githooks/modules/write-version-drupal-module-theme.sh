#!/usr/bin/env bash

# Writes the release version to a Drupal module or theme *.info.yml file.
# This script will be run if VERSION_FILE_DRUPAL is set. I do this in
# git-flow-hooks-config.sh
# This script finds the line in the supplied Drupal *.info.yml file with "version:"
# and updates the version number with the new release version number.

VERSION_PREFIX=$(git config --get gitflow.prefix.versiontag)

if [ ! -z "$VERSION_PREFIX" ]; then
    VERSION=${VERSION#$VERSION_PREFIX}
fi

if [ -z "$VERSION_BUMP_MESSAGE_DRUPAL" ]; then
    VERSION_BUMP_MESSAGE_DRUPAL="Bump Drupal module or theme version to %version%"
fi

sed -i "/^\([[:space:]]*version: \).*/s//\1\'$VERSION\'/" "$VERSION_FILE_DRUPAL" && \
    git add $VERSION_FILE_DRUPAL && \
    git commit -m "$(echo "$VERSION_BUMP_MESSAGE_DRUPAL" | sed s/%version%/$VERSION/g)"

if [ $? -ne 0 ]; then
    __print_fail "Unable to write version to $VERSION_FILE_DRUPAL."
    return 1
else
    return 0
fi
