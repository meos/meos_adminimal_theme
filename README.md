# MEOS Adminimal Subtheme
A MEOS subtheme of the Drupal Adminimal administration theme with modern minimalist design.

Because the use of the /sites/default/files/adminimal.css file is not yet available in the Drupal 8 Version of the Adminimal theme we created a subtheme to be able to make changes.
